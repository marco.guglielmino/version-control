# <span style="color: green;"><b>Guida git</b></span>
![Ecco un’immagine di esempio](https://www.20i.com/blog/wp-content/uploads/2022/08/git-blog-header.png)

## <span style="color: #93C572;">Comandi essenziali</span>

Ci sono alcuni comandi essenziali per poter usare git in modo abbastanza completo. Li divido in:

1. [Comandi base](#comandi-base)
2. [Comandi Branch](#comandi-branch)
2. [Comandi repository remote](#repository-remote)

***

<br>

### <span style="color: #93C572;">Comandi base

In </span>questa sezione troviamo alcuni comandi base per la creazione di una repository locale e il "salvataggio" delle modifiche ai file in essa.

<br>


```bash
git init 
```
Questo è il primo comando da sapere perchè ci permette di inizializzare una cartella a repository, creando al suo interno una cartella nascosta chiamata "`.git`". In essa salveremo
***
<br>


```bash
git add <nome file/cartella modificato>
```
Aggiunge il file/cartella modificato alla cartella "`.git`" o lo "sovrascrive" alla copia precendente già presente.
***

<br>


```bash
git add .
```
Può essere usato in alternativa al comando precedente per aggungere tutte le modifiche apportate nella cartella "padre"
***

<br>

```bash
git commit
```
Salva le modifiche al file/cartella e apre un editor dove inserire un commento che sarà il "titolo" della commit. Il commit avverrà sul branch attuale.
***

<br>

```bash
git commit -m 'commento'                         
```
Si può usare al posto del comando precedente ed evita il passaggio tramite l'editor per la scrittura del commento.
***

<br>

```bash
git log
```
Mostra uno storico dei commit effettuati su tutti i branch, con i rispettivi commenti, e ti mostra su che commit sei attualmente.
***

<br>

```bash
git log --all --decorate --oneline --graph
```
Comando più "grafico" del semplice "`git log`" per visualizzare tutti i commit. Differenze:

- <span style="color: #D1E231">git log</span>

```bash

commit fc42074dd46a6dd82af4e70acec07b375dcbdf53 (HEAD -> master)
Author: MarcoGugli <marco.guglielmino003@gmail.com>
Date:   Sat Apr 29 12:12:08 2023 +0200

    modifiche salvate

commit b07b96f0b99b3bbf22259ea80f2eeb2bf960bc43
Author: MarcoGugli <marco.guglielmino003@gmail.com>
Date:   Sat Apr 29 12:10:44 2023 +0200

    iniz prova2

commit 3fd196700fba3fc1dfb802144ca92a1cdf353e5e
Merge: baec369 63fb11d
Author: MarcoGugli <marco.guglielmino003@gmail.com>
Date:   Fri Apr 28 17:02:08 2023 +0200

    completato

commit baec3696d1801b532a0b86ef57261d36083ff3f6
Author: MarcoGugli <marco.guglielmino003@gmail.com>
Date:   Fri Apr 28 16:57:47 2023 +0200

    avanzamento desktop

commit 63fb11d0a87f7e8f0eb5300d86cedcb34b2b35cf (feat/mobile)
Author: MarcoGugli <marco.guglielmino003@gmail.com>
Date:   Fri Apr 28 16:55:21 2023 +0200
```

- <span style="color: #D1E231">git log --all --decorate --oneline --graph</span>

```bash
* 63b6515 (HEAD -> master) scrittura
* fc42074 modifiche salvate
* b07b96f iniz prova2
*   3fd1967 completato
|\
| * 63fb11d (feat/mobile) fine mobile
| * 54e69bd buon progresso
| * 4201827 inizio sviluppo mobile
* | baec369 avanzamento desktop
|/
* 534c935 eliminata giornata
* c1d24d7 bella giornata
* 8dae49e aggiunta saluto
* 306b08f aggiunta intestazione
```
***

<br>

```bash
git status
```
Dice su che branch sei attualmente e se c'è qualcosa da committare o da aggiungere alla cartella "`.git`".
***

<br><br>

### <span style="color: #93C572;">Comandi branch</span>

In questa sezione si trovano i comandi utili per la creazione, navigazione e merge dei branch
Un branch è un "ramo" che si stacca dal quello su cui siamo attualmente e prende una sua strada in cui verranno fatte altre modifiche che verranno successivamente mergiate.

<br>

```bash
git checkout <nome del branch o del commit su cui ci si vuole spostare>
```
Comando che serve per spostarsi tra i branch o i commit. Per spostarsi ad un commit precedente bisogna inserire i primi caratteri di esso. Es. `git checkout 63b6515`.
***

<br>

```bash
git checkout -b <nome del branch che si vuole creare>
```
Aggiungendo "-b" al comando precedente si crea un branch, con il nome specificato, dal branch in cui siamo attualmente. Ogni tanto potrebbe succedere che perdiamo le modifiche apportate ad un branch perché abbiamo usato il comando sbagliato: ivece di usare `git checkout -b` abbiamo usato `git checkout`. In questo caso la shell visualizza il seguente errore "Sei in detatched mode". Inoltre ci viene consigliato di creare un nuovo branch, mantenendo le modifiche fatte, con i seguenti comandi:

```bash
git switch
```
Comando che serve per spostarsi da un branch all’altro. Combinando `switch` e `checkout` ci si può spostare su un commit molto precedente rispetto a dove siamo ora e creare un nuovo branch partendo da li.

```bash
git switch -c
```
Aggiungendo "-c" al comando precedente si crea un branch "detatched".
***

<br>

#### <span style="color: #D1E231;">Merge</span>

Questo è uno dei comandi più potenti di git e serve per unire due branch:

```bash
git merge <nome del branch da unire a quello su cui ci troviamo>
```

Si parte dal branch su cui siamo attualemente e si unisce quello scritto nel comando. Nel caso in cui ci fossero dei conflitti durante il merge dei due branch viene visualizzato il seguente errore:

```bash
CONFLICT (content): Merge conflict in merge.txt
Automatic merge failed; fix conflicts and then commit the result.
```

In questo caso, come ci suggerisce l'errore, andiamo a fixare l'errore per esempio tramite il comando `nano` e andiamo successivamente a committare le modifiche. In questo modo avremo un unico branch con tutte le modifiche salvate.
***

<br><br>

### <h3 style="color: #93C572;">Repository remote</h3>

Tutte le modifiche e i salvataggi fatti fino ad ora sono locali, qundi altre persone del nostro gruppo non potrebbero accedervi; per questo motivi esistono delle piattaforme su cuoi possiamo caricare i nostri file e le nostre modifiche e renderle visibili a chi vogliamo. Possiamo fare tutto questo tramite dei comandi di git, "personalizzati" per ogni piattaforma.
***

<br>

#### <span style="color: #D1E231">Git lab</span>

Per prima cosa dobbiamo creare un nuovo progetto in cui andremo a caricare i nostri file.

Fatto questo usiamo il seguente comando per dire a git qual'è l'indirizzo su cui caricheremo le modifiche:

```bash
git remote add origin git@gitlab.com: <nome utente gitlab>/<nome del progetto>.git
```
***

<br>

```bash
git push -u origin --all
```
Con questo comando si sincronizzano i cambiamenti nell'origine alla repository remota.


```bash
git pull -all
```

Questo comando serve invece per aggiornare la nostra cartella locale con le modifiche più recenti del progetto